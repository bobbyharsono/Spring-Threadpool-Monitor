# Spring-Threadpool-Monitor

[![pipeline status](https://gitlab.com/bobbyharsono/Spring-Threadpool-Monitor/badges/master/pipeline.svg)](https://gitlab.com/bobbyharsono/Spring-Threadpool-Monitor/commits/master)

[![coverage report](https://gitlab.com/bobbyharsono/Spring-Threadpool-Monitor/badges/master/coverage.svg)](https://gitlab.com/bobbyharsono/Spring-Threadpool-Monitor/commits/master)

This is a small library for monitoring Threadpool Task Executor's status, i found out that when debugging or benchmarking application, often we need to know the condition of current pool 
while processing data. This tool is far from perfect but at least it will give some information you might need and i will try to update this in my spare time 

## Getting Started

Simply clone the repository and build it using Maven, you will get a jar file which you will include as dependency in your project

You can call the service by:

* Creating a singleton using Spring context


```
<bean id="threadpoolMonitorService"
		class="com.gitlab.bobbyharsono.monitor.threadpool.impl.ThreadpoolMonitorService" init-method="init" destroy-method="destroy">
		<property name="executors">
			<list>
				<ref bean="threadPoolTaskExecutor" />
			</list>
		</property>
		<property name="monitorInterval" value="3000" />
</bean>
```


* Calling it programatically
```
IThreadedMonitorService monitorService = new ThreadpoolMonitorService();
Thread monitor = new Thread(monitorService);
monitor.start();
```

### Prerequisites

Implemented using Oracle JDK 7


### Installing

Clone the project and build using Maven command

```
mvn clean package
```

Ship the jar as dependency of your program and you're good to go


## Running the tests

There are Junit test classes inside this to illustrate monitoring threaded task, simply run the Junit class ThreadpoolMonitorServiceTest, the result can be seen in log file. 
You can modify the log configuration in log4j2.xml file 


### Junit test explanation

Class ThreadpoolMonitorServiceTest is simply a main class which invoke bean in context file, there are only two things here:
1. ThreadedTaskRunnerAndMonitor will run a multithreaded task (ThreadedTask), it is just print the value of the job to the log
2. In the bean, there is threadpoolMonitorService, which has reference to Task Executor used by the job (ThreadedTask); So by having it as an attribute, system will monitor it within
3. given interval (3000 secs); Note that if you want to use this with scheduler like cron, set the interval = 0; This is to ensure no abnormal behavior within operation;

Just run the main test class and you should see the result in pre-defined log file, the default location is in the program's directory inside logs folder


## Deployment

Currently i haven't made any build config for this project, so you need to build it with yourself

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Bobby R. Harsono** - *Initial work* - [Gitlab](https://gitlab.com/bobbyharsono/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* https://pixabay.com/en/system-monitor-cpu-analysis-power-97892/