package com.gitlab.bobbyharsono.monitor.threadpool;

import java.util.List;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**************************************************************
 * @author Bobby
 * <p>
 *         This service will monitor collection of threads; This service itself
 *         also multi-threaded, allowing for invocation in specified interval
 *         without interrupting existing processes.
 * </p?     
 * **************************************************************/
public interface IThreadedMonitorService extends Runnable {

	void setExecutors(List<ThreadPoolTaskExecutor> threadPoolTaskExecutors);

	ThreadPoolTaskExecutor getExecutor(int index);

	void monitorExecutor();

}
