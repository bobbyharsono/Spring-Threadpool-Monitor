package com.gitlab.bobbyharsono.monitor.threadpool.impl;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.gitlab.bobbyharsono.monitor.threadpool.IThreadedMonitorService;

public class ThreadpoolMonitorService implements IThreadedMonitorService {

	private Logger logger = LogManager.getLogger(this.getClass().getSimpleName());
	private List<ThreadPoolTaskExecutor> executors;
	private int monitoringInterval;
	private final String SEPARATOR = "|";
	
	private Thread thread = new Thread(this);
	private volatile boolean running = true;

	public void init(){
		logger.info("Spring Threadpool Monitoring Service started");
		this.running = true;
		thread.start();
	}
	
	public void destroy(){
		this.running = false;
		logger.info("Spring Threadpool Monitoring Service stopped");
	}

	@Override
	public void run() {
		try{
			while(running){
				monitorExecutor();
				Thread.sleep(monitoringInterval);
			}
		}
		catch(InterruptedException interruptedException){
			interruptedException.printStackTrace();
			logger.error(interruptedException.getMessage(), interruptedException);
			Thread.currentThread().interrupt();
		}
	}
	

	@Override
	public void monitorExecutor() {
		if (!executors.isEmpty()) {
			StringBuffer buffer = new StringBuffer();
			for (ThreadPoolTaskExecutor taskExecutor : executors) {
				ThreadPoolExecutor executor = taskExecutor
						.getThreadPoolExecutor();

				buffer.append("ThreadNamePrefix ")
						.append(taskExecutor.getThreadNamePrefix())
						.append(SEPARATOR);
				buffer.append("CurrentPoolSize ")
						.append(executor.getPoolSize()).append(SEPARATOR);
				buffer.append("CorePoolSize ")
						.append(executor.getCorePoolSize()).append(SEPARATOR);
				buffer.append("MaxPoolSize ")
						.append(executor.getMaximumPoolSize())
						.append(SEPARATOR);
				buffer.append("ActiveTaskCount ")
						.append(executor.getActiveCount()).append(SEPARATOR);
				buffer.append("CompletedTaskCount ")
						.append(executor.getCompletedTaskCount())
						.append(SEPARATOR);
				buffer.append("TotalTaskCount ")
						.append(executor.getTaskCount()).append(SEPARATOR);
				buffer.append("IsTerminated ").append(executor.isTerminated())
						.append(SEPARATOR);
				
				logger.info(buffer.toString());
			}
		} else {
			logger.info("There are no [ThreadpoolTaskExecutor] currently being monitored");
		}

	}

	@Override
	public ThreadPoolTaskExecutor getExecutor(int index) {
		ThreadPoolTaskExecutor executor = null;
		if (index >= 0) {
			executor = executors.get(index);
		} else {
			logger.error("There is no [ThreadPoolTaskExecutor] with index below 0");
		}
		return executor;
	}

	public int getMonitorInterval() {
		return monitoringInterval;
	}

	public void setMonitorInterval(int monitoringInterval) {
		this.monitoringInterval = monitoringInterval;
	}

	@Override
	public void setExecutors(
			List<ThreadPoolTaskExecutor> threadPoolTaskExecutors) {
		this.executors = threadPoolTaskExecutors;
	}

}
