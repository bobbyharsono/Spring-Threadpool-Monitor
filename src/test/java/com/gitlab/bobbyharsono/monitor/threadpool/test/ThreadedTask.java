package com.gitlab.bobbyharsono.monitor.threadpool.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreadedTask implements Runnable {
	private Logger logger = LogManager.getLogger(this.getClass().getSimpleName());
	private String name;

	private boolean done;

	public ThreadedTask() {

	}

	public ThreadedTask(String name) {
		this.name = name;
		this.done = false;
	}

	@Override
	public void run() {
		try {
			logger.info("Task [{}] is started", this.name);
			logger.info("Task [{}] is completed", this.name);
		} finally {
			this.done = true;
		}
	}

	@Override
	public String toString() {
		return this.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

}
