package com.gitlab.bobbyharsono.monitor.threadpool.test;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadedTaskRunnerAndMonitor {

	private Logger logger = LogManager.getLogger(this.getClass().getSimpleName());
	
	private ThreadPoolTaskExecutor executor;

	private int threadCount;

	public void init(){
		logger.info("Initiating ThreadpoolMonitorService. . .");
	}
	
	public void destroy(){
		logger.info("Destroying ThreadpoolMonitorService. . .");
	}
	
	public void start() {
		logger.info("It has started");
		
		try {
			for(int i=0;i<threadCount;i++){
				ThreadedTask t = new ThreadedTask("Task-"+String.valueOf(new Random().nextInt()));
				executor.execute(t);
			}

			Thread.sleep(1500);
			
			for(int i=0;i<threadCount;i++){
				ThreadedTask t = new ThreadedTask("Task-"+String.valueOf(new Random().nextInt()));
				executor.execute(t);
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally{
			executor.shutdown();
		}
	}
	
	public ThreadPoolTaskExecutor getExecutor() {
		return executor;
	}

	public void setExecutor(ThreadPoolTaskExecutor executor) {
		this.executor = executor;
	}

	public int getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}

}
