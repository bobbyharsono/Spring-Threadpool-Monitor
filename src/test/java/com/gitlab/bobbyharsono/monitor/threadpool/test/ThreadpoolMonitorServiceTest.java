package com.gitlab.bobbyharsono.monitor.threadpool.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.gitlab.bobbyharsono.monitor.threadpool.impl.ThreadpoolMonitorService;

@ContextConfiguration(locations = "/threadpool-monitoring-context-test.xml")
@DirtiesContext(classMode=ClassMode.AFTER_CLASS) 
public class ThreadpoolMonitorServiceTest extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	private ThreadpoolMonitorService threadpoolMonitorService;

	public ThreadpoolMonitorService getThreadpoolMonitorService() {
		return threadpoolMonitorService;
	}

	public void setThreadpoolMonitorService(ThreadpoolMonitorService threadpoolMonitorService) {
		this.threadpoolMonitorService = threadpoolMonitorService;
	}

	@Before
	public void init() {
		System.out.println("\n============= ThreadpoolMonitorServiceTest =================");
	}

	@Test
	public void keepRunning() {
		int counter = 0 ;
		while (counter<20000) {
			try {
				Thread.sleep(2000);
				counter+=2000;
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
